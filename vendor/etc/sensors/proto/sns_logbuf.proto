// @file sns_logbuf.proto
//
// Defines sensor debug API message types for physical sensors.
//
// All physical Sensor drivers are required to use this API to support debug function.
//
// Copyright (c) 2021-2021 Technologies, Inc.
// All Rights Reserved.
// Confidential and Proprietary - Technologies, Inc.

syntax = "proto2";
import "nanopb.proto";

enum sns_logbuf_msgid
{
	option (nanopb_enumopt).long_names = false;

	// send log msg to ap
	SNS_SEND_LOGBUF_MSG_TO_AP_EVENT = 1200;
	// send logbuf flush msg to slpi
	SNS_SEND_LOGBUF_FLUSH_EVENT = 1201;
	// logbuf timestamp sync(ap to slpi)
	SNS_SEND_LOGBUF_TIMESTAMP_EVENT = 1202;
	// logbuf loglevel and mask(ap to slpi)
	SNS_SEND_LOGBUF_LOG_INFO_EVENT = 1203;
}

// send log msg to ap_event
message send_logbuf_msg_to_ap_event
{
	// logbuf block
	required string logbuf_block = 1;
}

message sns_logbuf_timestamp
{
	required uint32 year = 1;
	required uint32 mon = 2;
	required uint32 day = 3;
	required uint32 h = 4;
	required uint32 min = 5;
	required uint32 sec = 6;
	required uint32 ms = 7;
	required uint64 us = 8;
}

enum sns_logbuf_log_info_type {
    option (nanopb_enumopt).long_names = false;

    SNS_LOGBUF_TYPE_LOG_LEVEL_INFO = 1;
    SNS_LOGBUF_TYPE_LOG_MASK_INFO = 2;
}

message sns_logbuf_log_config_info {
    /* A value from enum qsh_wwan_cell_info_type indicating the radio access
     * technology of the cell, and which one of the cellinfo fields below can
     * be used to retrieve additional information. */
    required sns_logbuf_log_info_type sns_log_info_type = 1;

    oneof sns_log_info {
        uint32 loglevel = 2;
        uint32 logmask = 3;
    }
}
